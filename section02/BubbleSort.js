function bubbleSort(aInput){
    for(var i =0;i < aInput.length - 1; i++){
    // loop whole array
    //except the last i element
    // i represents the number of already sorted elements
    for(var j = 0; j < aInput.length - i - 1; j++){
        // Swap
        //a, b
        if (aInput[j] > aInput[j + 1]) {
            var temp = aInput[j];
            aInput[j] = aInput[j + 1];
            aInput[j + 1] = temp;
           }
        }
    }
}

// same as bubble sort
// checkk for swap
// if no swap happened
// end the algorithm (return)


var aInput = [100,2,34,67,99,43,57,92,56,7,1,55];
console.log(aInput);

bubbleSort(aInput);
console.log(aInput);