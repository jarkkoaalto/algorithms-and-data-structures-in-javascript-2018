function quickSort(aInput,from,to){
    if(from<to){
        var indexOfPivot = partition(aInput, from,to); // return the index of pivot
        quickSort(aInput,from, indexOfPivot - 1);
        quickSort(aInput, indexOfPivot + 1, to);
    }
}

function partition(aInput,from,to){
    var pivot = aInput[to];
    var wall = from; // pointing at first number behind our "wall"
    for(var i = from; i<to; i++){
        if(aInput[i] <= pivot){
            //swap with first number behind our wall
            var temp = aInput[wall];
            aInput[wall] = aInput[i];
            aInput[i] = temp;
            // moving wall
            wall++;
        }
    }
    aInput[to] = aInput[wall];
    aInput[wall] = pivot;
    return wall;
}


var aInput = [100,34,78,2,8,34,9,5,54];
console.log(aInput);

quickSort(aInput,0,aInput.length -1);
console.log(aInput);

