// 10! = 10*9*8*7*6*5*4*3*2*1

function factorial(n){
    var result = n;
    while(n > 1){
        n--;
        result = result * n;
    }
    return result;
}

function recursiceFactorial(n){
    console.log("Computing factorial of " + n)
    if(n <= 1){
        return 1;
    }else{
        var temp = recursiceFactorial(n - 1);
        console.log("The result of factorial of " + (n - 1) + " is " +temp);
        return n * temp;
    }

}

console.log(factorial(10)); // 3628800


console.log(recursiceFactorial(10)); // 3628800 
// 10 * f(9)       = 3628800
// f(9) = 9 * f(8) = 362880
// f(8) = 8 * f(7) = 40320
// f(7) = 7 * f(6) = 720
// f(6) = 6 * f(5) = 120
//        .
//        .
//        .
// f(2) = 2 * f(1) => 2
// f(1) = 1 