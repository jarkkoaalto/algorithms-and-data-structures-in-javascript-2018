function selectSort(aInput){
    for(var wall = 0; wall < aInput.length -1; wall++){
        // find smallest number
        // wall - index, points at first number behind "wall"
        var indexOfSmallest = wall;
        for (var x = wall + 1; x < aInput.length; x++) {
            if (aInput[indexOfSmallest] > aInput[x])
                indexOfSmallest = x; // change smallest in x

        }
        // Smallest element is on IndexOfSmallest
        // swap
        var temp = aInput[wall];
        aInput[wall] = aInput[indexOfSmallest];
        aInput[indexOfSmallest] = temp;
    }
   
    
}

var aInput = [10,25,23,5,1,1,3,22,65];
console.log(aInput);
// Sorted Array
selectSort(aInput);
console.log(aInput);