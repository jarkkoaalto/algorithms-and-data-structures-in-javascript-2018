function bubbleSort(aInput) {
    for (var i = 0; i < aInput.length - 1; i++) {
        // loop whole array
        //except the last i element
        // i represents the number of already sorted elements
        for (var j = 0; j < aInput.length - i - 1; j++) {
            // Swap
            //a, b
            if (aInput[j] > aInput[j + 1]) {
                var temp = aInput[j];
                aInput[j] = aInput[j + 1];
                aInput[j + 1] = temp;
            }
        }
    }
}

// same as bubble sort
// checkk for swap
// if no swap happened
// end the algorithm (return)
//var aInput = [100, 2, 34, 67, 99, 43, 57, 92, 56, 7, 1, 55];
// console.log(aInput);
// var aInput = [];
bubbleSort(aInput);
// console.log(aInput);


// Cede challenge

// same as bubble sort
// checkk for swap
// if no swap happened
// end the algorithm (return)

function bubbleSortOptimized(aInput) {
    for (var i = 0; i < aInput.length - 1; i++) {
        // loop whole array
        //except the last i element
        // i represents the number of already sorted elements
        var swapHappened = false;
        for (var j = 0; j < aInput.length - i - 1; j++) {
            // Swap
            //a, b
            if (aInput[j] > aInput[j + 1]) {
                swapHappened = true;
                var temp = aInput[j];
                aInput[j] = aInput[j + 1];
                aInput[j + 1] = temp;
            }
        }
        if (!swapHappened) {// if(swapHappened == false)
            // the array is 
            return;
        }
    }
}

// var aInput = [100, 2, 34, 67, 99, 43, 57, 92, 56, 7, 1, 55];
// console.log(aInput);

bubbleSortOptimized(aInput);
// console.log(aInput);

function selectSort(aInput) {
    for (var wall = 0; wall < aInput.length - 1; wall++) {
        // find smallest number
        // wall - index, points at first number behind "wall"
        var indexOfSmallest = wall;
        for (var x = wall + 1; x < aInput.length; x++) {
            if (aInput[indexOfSmallest] > aInput[x])
                indexOfSmallest = x; // change smallest in x

        }
        // Smallest element is on IndexOfSmallest
        // swap
        var temp = aInput[wall];
        aInput[wall] = aInput[indexOfSmallest];
        aInput[indexOfSmallest] = temp;
    }


}

//var aInput = [10, 25, 23, 5, 1, 1, 3, 22, 65];
// console.log(aInput);
// Sorted Array
selectSort(aInput);
// console.log(aInput);

function mergeSort(aInput, from, to){
    if (to - from < 1) {
        // end
        return;
    } else {
        var middle = ((to + from) / 2);
        // this two line make sure sorting
        mergeSort(aInput, from, middle);
        mergeSort(aInput, middle + 1, to);

        merge(aInput, from, middle, to);
    }
}

function merge(aOutput, from, middle, to) {
    var left = aOutput.slice(from, middle + 1);
    var right = aOutput.slice(middle + 1, to + 1);
    var leftPointer = 0;
    var rightPointer = 0;
    for (var i = from; i <= to; i++) {
        if (leftPointer === left.length) {
            while (rightPointer < right.length) {
                aOutput[i] = right[rightPointer];
                rightPointer++;
                i++;
            }
            return;
        }

        if (rightPointer === right.length) {
            while (leftPointer < left.length) {
                aOutput[i] = left[leftPointer];
                leftPointer++;
                i++;
            }
            return;
        }

        if (left[leftPointer] <= right[rightPointer]) {
            aOutput[i] = left[leftPointer];
            leftPointer++;
        } else {
            aOutput[i] = right[rightPointer];
            rightPointer++;
        }
    }
}
// var aInput = [100, 20, 14, 65, 87, 1, 33, 60, 23];
// console.log(aInput);



mergeSort(aInput, 0, aInput.length - 1);
// console.log(aInput);


function quickSortExecutor(aInput){
    quickSortExecutor(aInput, 0, aInput.length - 1);
}

function quickSortMedianExecutor(aInput){
    quickSortMedianExecutor(aInput,0,aInput.length -1);
}

function mergeSortExecutor(aInput){
    mergeSort(aInput,0,aInput.length -1);
}

function isSorted(aInput){
    for(var i =0; i<aInput.length - 1;i++){
        if(aInput[i] < aInput[i+1]){
            return false;
        }
    }

    return true;
}

var NUMBER_OF_ARRAYS = 100;
var ELEMENTS_PER_ARRAY = 1000;

var aSortingAlgorithms  = [bubbleSort,selectSort,
    bubbleSortOptimized, mergeSortExecutor,quickSortExecutor, quickSortMedianExecutor]

for(var k = 0; k<aSortingAlgorithms.length; k++){
    var start = new Date().getTime();
    for(var i = 0; i<NUMBER_OF_ARRAYS; i++){
        var aInput = [];
        for(var j = 0; j<ELEMENTS_PER_ARRAY; j++){
            aInput[j] =Math.floor(Math.random() * 100);
        }
        aSortingAlgorithms[k](aInput);
        if(!isSorted(aInput)){
            console.log("ERROR: Arrays on sorted"  + aInput);
        }
    }
    var end = new Date().getTime();
    console.log(k + " took: " + (end - start) + " ms");
}