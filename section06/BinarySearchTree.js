function Node(iValue,oLeft, oRight){
    this.value = iValue;
    this.left = oLeft;
    this.right = oRight;
}

var BST = {
    root: undefined,
    insert: function(iValue){
        if(this.root === undefined){
            this.root = new Node(iValue);
            return true;
        }
        var oNodeBefore = undefined;
        var oCurrentNode = this.root;
        while(oCurrentNode !== undefined){
            oNodeBefore = oCurrentNode;
            if(oCurrentNode.value < iValue){
                oCurrentNode = oCurrentNode.right;
            }else if(oCurrentNode.value > iValue){
                oCurrentNode = oCurrentNode.left;
            }else {
                 return false;
            }
        }
        if(oNodeBefore.value < iValue){
            oNodeBefore.right = new Node(iValue);
        }else{
            oNodeBefore.left = new Node(iValue);
        }
        return true;
    },

    search: function (iValue) {
        var oCurrentNode = this.root;
        while (oCurrentNode !== undefined) {
            if (oCurrentNode.value < iValue) {
                oCurrentNode = oCurrentNode.right;
            }
            else if (oCurrentNode.value > iValue) {
                oCurrentNode = oCurrentNode.left;
            }
            else {
                return true;
            }
        }
        return false;
    }
}

var aInput = [];
for(var i = 0; i < 100; i++){
    aInput[i] = Math.round(Math.random() * 100);
    BST.insert(aInput[i]);
}
for(var i = 0; i < 100; i++){
    if(!BST.search(aInput[i])){
        console.log("Error: Value is not the tree " + aInput[i]);
    }
}
console.log("END");
