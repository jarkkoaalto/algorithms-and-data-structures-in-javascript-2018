// Cede challenge

// same as bubble sort
// checkk for swap
// if no swap happened
// end the algorithm (return)

function bubbleSortOptimized(aInput) {
    for (var i = 0; i < aInput.length - 1; i++) {
        // loop whole array
        //except the last i element
        // i represents the number of already sorted elements
        var swapHappened = false;
        for (var j = 0; j < aInput.length - i - 1; j++) {
            // Swap
            //a, b
            if (aInput[j] > aInput[j + 1]) {
                swapHappened = true;
                var temp = aInput[j];
                aInput[j] = aInput[j + 1];
                aInput[j + 1] = temp;
            }
        }
         if(!swapHappened) {// if(swapHappened == false)
            // the array is 
            return;
         }
    }
}




var aInput = [100, 2, 34, 67, 99, 43, 57, 92, 56, 7, 1, 55];
console.log(aInput);

bubbleSortOptimized(aInput);
console.log(aInput);