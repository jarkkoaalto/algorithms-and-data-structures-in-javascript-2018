function quickSortImp(aInput, from, to) {
    if (from < to) {
        var indexOfPivot = partitionImp(aInput, from, to); // return the index of pivot
        quickSortImp(aInput, from, indexOfPivot - 1);
        quickSortImp(aInput, indexOfPivot + 1, to);
    }
}
function median(aInput,first,second, third){
    var SmallthanSecond = (aInput[first] < aInput[second]);
    var SmallthanThird = (aInput[second] < aInput[third]);
    if(SmallthanSecond && SmallthanThird) {
        return second;
    }
    if(!SmallthanSecond && !SmallthanThird){
        return second;
    }
    if(!SmallthanSecond && SmallthanThird){
        if((aInput[first] < aInput[third])){
            return first;
        }else{
            return third;
        }
    }
    if (SmallthanSecond && !SmallthanThird) {
        if ((aInput[first] < aInput[third])) {
            return third;
        } else {
            return first;
        }
    }
}


function partitionImp(aInput, from, to) {
    var indexOfPivot = median(aInput, from, parseInt((from+to)) /2, to);
    var pivot = aInput[indexOfPivot];
    if(indexOfPivot !== to){
        aInput[indexOfPivot] == aInput[to];
    }
    var wall = from; // pointing at first number behind our "wall"
    for (var i = from; i < to; i++) {
        if (aInput[i] <= pivot) {
            //swap with first number behind our wall
            var temp = aInput[wall];
            aInput[wall] = aInput[i];
            aInput[i] = temp;
            // moving wall
            wall++;
        }
    }
    aInput[to] = aInput[wall];
    aInput[wall] = pivot;
    return wall;
}

// median fo 3 values
// 3 random index from, (from + to) / 2;


var aInput = [100, 34, 78, 2, 8, 34, 9, 5, 54];
console.log(aInput);

quickSortImp(aInput, 0, aInput.length - 1);
console.log(aInput);

